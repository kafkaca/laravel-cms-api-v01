@extends('layouts.general_layout', array())
@section('custom_js')
@endsection
@section('content')
@include('headers.general_header')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
@include('widgets.general_sidebar', array('title' => ''))
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row">
        <div class="col-lg-12">

                          <div class="row">

                            @for($i = 0; $i < 12; $i ++)


                                <div class="col-lg-4 col-md-6">
                                  <div class="panel panel-body">
                                    <div class="media">
                                      <div class="media-left">
                                        <a href="/assets/images/placeholder.jpg" data-popup="lightbox">
                                          <img src="/assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
                                        </a>
                                      </div>

                                      <div class="media-body">
                                        <h6 class="media-heading">James Alexander</h6>
                                        <span class="text-muted">Lead developer</span>
                                      </div>

                                      <div class="media-right media-middle">
                                        <ul class="icons-list icons-list-vertical">
                                                    <li class="dropdown">
                                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                                      <ul class="dropdown-menu dropdown-menu-right">
                                              <li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
                                              <li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
                                              <li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
                                              <li class="divider"></li>
                                              <li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
                                            </ul>
                                                    </li>
                                                  </ul>
                                      </div>
                                    </div>
                                  </div>
                                </div>



                            @endfor
                            <style media="screen">
                              .caption{
                                position: relative;
              bottom: 15px;
              background-color: white;
                              }
                            </style>



                          </div>

      </div>

    </div>
    <!-- /detailed task -->
  </div>
  <!-- /main content -->
</div>
<!-- /page content -->
</div>
<!-- /page container -->
@include('footers.general_footer')
@endsection
