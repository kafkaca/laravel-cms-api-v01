@extends('layouts.general_layout', array())
@section('custom_js')
<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="assets/js/pages/form_inputs.js"></script>
<script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>
<script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="assets/js/pages/uploader_bootstrap.js"></script>
<!-- /theme JS files -->
@endsection
@section('content')
@include('headers.dashboard')

<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		@include('widgets.kampanya_sidebar', array('title' => 'classified'))
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Detailed task -->
			<div class="row">


				<div class="col-lg-12" style="">

					<!-- Form horizontal -->
  				<div class="panel panel-flat">
  					<div class="panel-heading">
  						<h5 class="panel-title">Yeni Kampanya</h5>
  						<div class="heading-elements">
  							<ul class="icons-list">
  		                		<li><a data-action="collapse"></a></li>
  		                		<li><a data-action="reload"></a></li>
  		                		<li><a data-action="close"></a></li>
  		                	</ul>
  	                	</div>
  					</div>

  					<div class="panel-body">
							<form class="form-horizontal" action="/api/kampanya-ekle" method="post">
							  <fieldset class="content-group">
							    <div class="form-group">
							      <label class="control-label col-lg-2">Kampanya Adı</label>
							      <div class="col-lg-10">
							        <input name="name" type="text" class="form-control" placeholder="örn: ayakkabı sezonu">
							      </div>
							    </div>
							    <div class="form-group">
							      <label class="control-label col-lg-2">Kampanya Türü</label>
							      <div class="col-lg-10">
							        <select name="type" class="form-control">
							          <option value="banner">Banner</option>
							          <option value="feed">Feed</option>
							        </select>
							      </div>
							    </div>
							    <div class="form-group">
							      <label class="control-label col-lg-2">Notlar</label>
							      <div class="col-lg-10">
							        <textarea name="note" rows="5" cols="5" class="form-control" placeholder="Default textarea"></textarea>
							      </div>
							    </div>
							  </fieldset>
							    <input name="user_id" type="hidden" value="{!! getUserInfo()['user_id'] !!}">
							  <div class="text-right">
							    <button type="submit" class="btn btn-primary">Gönder <i class="icon-arrow-right14 position-right"></i></button>
							  </div>
							</form>
  					</div>
  				</div>
  				<!-- /form horizontal -->






					</div>

				</div>
				<!-- /detailed task -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->
	@endsection
