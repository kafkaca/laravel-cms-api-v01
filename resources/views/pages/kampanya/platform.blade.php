@extends('layouts.general_layout', array())
@section('custom_js')
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
  <script type="text/javascript" src="assets/js/pages/form_inputs.js"></script>
<script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>
<script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="assets/js/pages/uploader_bootstrap.js"></script>
<!-- /theme JS files -->
@endsection
@section('content')
@include('headers.dashboard')

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">
          @include('widgets.kampanya_sidebar', array('title' => 'classified'))
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row">


        <div class="col-lg-12" style="">

          <!-- Form horizontal -->
  				<div class="panel panel-flat">
  					<div class="panel-heading">
  						<h5 class="panel-title">Platform</h5>
  						<div class="heading-elements">
  							<ul class="icons-list">
  		                		<li><a data-action="collapse"></a></li>
  		                		<li><a data-action="reload"></a></li>
  		                		<li><a data-action="close"></a></li>
  		                	</ul>
  	                	</div>
  					</div>

  					<div class="panel-body">
<form class="form-horizontal" action="/api/kampanyaekle" method="post">

	<div class="form-group">
		<label class="control-label col-lg-2">Browser</label>
		<div class="col-lg-10">
										<select multiple="multiple" class="form-control">
												<option selected="selected">Chrome</option>
												<option selected="selected">Firefox</option>
												<option>Yandex</option>
												<option selected="selected">Internet Explorer</option>

										</select>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-lg-2">Network</label>
		<div class="col-lg-10">
										<select multiple="multiple" class="form-control">
												<option selected="selected">Mobil</option>
												<option selected="selected">Wifi</option>
												<option>Lan</option>
												<option selected="selected">Intranet</option>

										</select>
		</div>
	</div>

  							<div class="text-right">
  								<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
  							</div>
  						</form>
  					</div>
  				</div>
  				<!-- /form horizontal -->






      </div>

    </div>
    <!-- /detailed task -->
  </div>
  <!-- /main content -->
</div>
<!-- /page content -->
</div>
<!-- /page container -->
@endsection
