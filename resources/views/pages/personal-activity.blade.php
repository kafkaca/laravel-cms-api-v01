@extends('layouts.general_layout', array())
@section('custom_js')
<script type="text/javascript" src="assets/js/pages/support_chat_layouts.js"></script>
<script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>
@endsection
@section('content')
@include('headers.personal_header')

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row">
        <div class="col-lg-9">
          @include('parts.for_articles', array('title' => ''))
        </div>
        <div class="col-md-3">
          <style media="screen">
            .col-md-3 .thumbnail{
                  /*margin-top: -220px;*/
            }
          </style>
          @include('parts.right_menu', array('title' => ''))
          <div class="panel">
  <div class="panel-body text-center">
    <div class="icon-object border-success-400 text-success"><i class="icon-book"></i></div>
    <h5 class="text-semibold">Knowledge Base</h5>
    <p class="content-group">Ouch found swore much dear conductively hid submissively hatchet vexed far inanimately alongside candidly much and jeez</p>
    <p><a href="#" class="btn bg-success-400">Browse articles</a></p>
  </div>
</div>
        </div>


      </div>

    </div>
    <!-- /detailed task -->
  </div>
  <!-- /main content -->
</div>
<!-- /page content -->
</div>
<!-- /page container -->
@endsection
