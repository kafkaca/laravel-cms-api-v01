<!-- Secondary sidebar -->
<div class="sidebar sidebar-main sidebar-default sidebar-separate">

  <div class="sidebar-content">

    <!-- Sidebar search -->
    <div class="sidebar-category">
      <div class="category-title">
        <span>Search</span>
        <ul class="icons-list">
          <li><a href="#" data-action="collapse"></a></li>
        </ul>
      </div>

      <div class="category-content">
        <form action="#">
          <div class="has-feedback has-feedback-left">
            <input type="search" class="form-control" placeholder="Search">
            <div class="form-control-feedback">
              <i class="icon-search4 text-size-base text-muted"></i>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- /sidebar search -->

    <!-- Action buttons -->
    <div class="sidebar-category">
      <div class="category-title">
        <span>Action buttons</span>
        <ul class="icons-list">
          <li><a href="#" data-action="collapse"></a></li>
        </ul>
      </div>

      <div class="category-content">
        <div class="row">
          <div class="col-xs-6">
            <button class="btn bg-teal-400 btn-block btn-float btn-float-lg" type="button"><i class="icon-git-branch"></i> <span>Branch</span></button>
            <button class="btn bg-purple-300 btn-block btn-float btn-float-lg" type="button"><i class="icon-mail-read"></i> <span>Messages</span></button>
          </div>

          <div class="col-xs-6">
            <button class="btn bg-warning-400 btn-block btn-float btn-float-lg" type="button"><i class="icon-stats-bars"></i> <span>Statistics</span></button>
            <button class="btn bg-blue btn-block btn-float btn-float-lg" type="button"><i class="icon-people"></i> <span>Users</span></button>
          </div>
        </div>
      </div>
    </div>
    <!-- /action buttons -->





  </div>
</div>
<!-- /secondary sidebar -->
