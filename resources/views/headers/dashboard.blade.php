<body class="navbar-bottom navbar-top layout-boxed">
  <!-- Page header -->
  <div class="page-header page-header-inverse bg-indigo">

    @include('parts.header_menu')


    <!-- Page header content -->
    <div class="page-header-content">
      <div class="page-title">
        <h4>Admin Dashboad</h4>
        <div class="heading-elements">
          <div class="btn-group">
            <button type="button" class="btn btn-default btn-raised"><b><i class="icon-make-group position-left"></i></b> Aslanlar Ticaret </button>
            <button type="button" class="btn btn-default btn-raised dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
            <ul class="dropdown-menu dropdown-menu-right">
              <li><a href="#"><i class="icon-menu7"></i> Ticaret Merkezi</a></li>
              <li><a href="#"><i class="icon-screen-full"></i> Ticaretin Sırları</a></li>
              <li><a href="#"><i class="icon-mail5"></i> Dünya Turu</a></li>
              <li class="divider"></li>
              <li><a href="#"><i class="icon-gear"></i> Sayfa Oluştur</a></li>
            </ul>
          </div>
        </div>

      </div>

    </div>
    <!-- /page header content -->
    @include('parts.dashboard_manage')
  </div>
  <!-- /page header -->
