<div class="row">

  @for($i = 0; $i < 12; $i ++)

  <div class="col-lg-4 col-sm-6">
    <div class="thumbnail">
      <div class="thumb">
        <img src="assets/images/placeholder.jpg" alt="">
        <div class="caption-overflow">
          <span>
            <a href="assets/images/placeholder.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
            <a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
          </span>
        </div>
      </div>

      <div class="caption">
        <h6 class="no-margin-top text-semibold"><a href="#" class="text-default">For ostrich much</a> <a href="#" class="text-muted"><i class="icon-download pull-right"></i></a></h6>
        Some various less crept gecko the jeepers dear forewent far the ouch far a incompetent
      </div>
    </div>
  </div>
@endfor
</div>
