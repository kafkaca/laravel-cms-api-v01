!function(t,e){"object"==typeof exports&&"object"==typeof module?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.scriber=e():t.scriber=e()}(this,function(){return function(t){function e(n){if(r[n])return r[n].exports;var o=r[n]={exports:{},id:n,loaded:!1};return t[n].call(o.exports,o,o.exports,e),o.loaded=!0,o.exports}var r={};return e.m=t,e.c=r,e.p="",e(0)}([function(t,e){function r(t){function e(t){return function(){return l.element(t)}}function n(t){return function(e){return l.attr(t,e)}}this.parent=null;var a=t,u=[],s=[],l=this;this.attr=function(t,e){return s[t]=e,this},this.element=function(t){var e=new r(t);return e.parent=this,u.push(e),e};for(var c=0;c<o.length;c++)this[o[c]]=e(o[c]);for(c=0;c<i.length;c++)this[i[c]]=n(i[c]);this.end=function(){return this.parent},this.toHTMLElement=function(){var t=document.createElement(a);for(var e in s)t.setAttribute(e,s[e]);for(var r=null,n=0;n<u.length;n++)r=u[n],"string"==typeof r?t.appendChild(document.createTextNode(r)):t.appendChild(u[n].toHTMLElement());return t},this.text=function(t){return u.push(t),this},this.toString=function(){return this.toHTMLElement().outerHTML}}function n(t){return function(){return a.element(t)}}var o=["a","abbr","acronym","address","applet","area","article","aside","audio","b","base","basefont","bdi","bdo","big","blockquote","body","br","button","canvas","caption","center","cite","code","col","colgroup","datalist","dd","del","details","dfn","dialog","dir","div","dl","dt","em","embed","fieldset","figcaption","figure","font","footer","form","frame","frameset","h1",NaN,"head","header","hr","html","i","iframe","img","input","ins","kbd","keygen","label","legend","li","link","main","map","mark","menu","menuitem","meta","meter","nav","noframes","noscript","object","ol","optgroup","option","output","p","param","pre","progress","q","rp","rt","ruby","s","samp","script","section","select","small","source","span","strike","strong","style","sub","summary","sup","table","tbody","td","textarea","tfoot","th","thead","time","title","tr","track","tt","u","ul","var","video","wbr"],i=["alt","diabled","href","src","title","value","name","id","class"],a={};a.element=function(t){var e=new r(t);return e.parent=e,e};for(var u=0;u<o.length;u++)a[o[u]]=n(o[u]);t.exports=a}])});



    (function() {
    "use strict";

    function showAds(){

        let datam = window.scriber
            .div()
                .id("myId")
                .class("my-css-class")
                .input()
                    .value("my value")
                .end()
                .element("span")
                .class("red")
                .end()
            .end()
            .toString();

      let me = document.currentScript;
      let attr = me.getAttribute("data-publisher");
      let ad_data = {
    	  url : 'http://demos-php.esy.es/sponsor/demo',
    	  width : 300,
    	  height : 435
      }

      me.insertAdjacentHTML('afterend', datam);
    }

    showAds();

      }());
