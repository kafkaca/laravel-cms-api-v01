<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}



$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);
/*
$app->withFacades(true, [
    Illuminate\Support\Facades\Session::class => "Session",
]);

*/
$app->withFacades();

$app->withEloquent();


/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton('filesystem', function ($app) {
    return $app->loadComponent('filesystems', Illuminate\Filesystem\FilesystemServiceProvider::class, 'filesystem');
});


/*
$app->singleton(
    Illuminate\Contracts\Support\Session::class,
    Symfony\Component\HttpFoundation\Session\Session::class
);
$app->resolving(Illuminate\Http\Request::class, function ($request, $app) {
    $request->setSession($app->session);
});*/
/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

// $app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
// ]);

// $app->routeMiddleware([
//     'auth' => App\Http\Middleware\Authenticate::class,
// ]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/
$app->register(Illuminate\Redis\RedisServiceProvider::class);
//$app->register(App\Providers\SessionServiceProvider::class);
// $app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);



$app->routeMiddleware([
     'auth' => App\Http\Middleware\Authenticate::class,
     //'dem' => App\Http\Middleware\AuthToken::class,
     //'auth' => App\Http\Middleware\Authenticate::class
     //'dem' => App\Http\Middleware\Authenticate::class,
]);

$app->register(Yega\Auth\JWTAuthServiceProvider::class);
$app->bind(Illuminate\Session\SessionManager::class, function ($app) {
    return new Illuminate\Session\SessionManager($app);
});

$app->middleware([
    App\Http\Middleware\ApiMiddleware::class,
    Illuminate\Session\Middleware\StartSession::class,
]);

$app->register(Illuminate\Session\SessionServiceProvider::class);

$app->group(['prefix' => 'api','namespace' => 'App\Http\Controllers\Api'], function ($app) {
    require __DIR__.'/../routes/api.php';
});

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../routes/web.php';
});



$app->configure('session');
$app->configure('filesystems');
$app->configure('cache');
$app->configure('database');
return $app;
