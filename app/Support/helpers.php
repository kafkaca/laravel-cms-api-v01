<?php
//https://laravel.com/docs/5.4/helpers


function convert($size)
{
  $unit=array('b','kb','mb','gb','tb','pb');
  return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

function getMemory()
{
  return convert(memory_get_usage(true));
}

function is_serial($string) {
  return (@unserialize($string) !== false || $string == 'b:0;');
}

function getUserInfo() {
  $user_id = $_COOKIE['user_id'];
  $user_name = $_COOKIE['user_name'];
  $info_array = [
    'user_id' => $user_id,
    'user_name' => $user_name
];
  return $info_array;
}


function bannerUpload($file){
  $path = base_path('public/uploads/images/');
  $fileName = base64_encode($file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();
  $dosyaadi = $path . $fileName;
  if (file_exists($dosyaadi)) {
    unlink($dosyaadi);
  }
  $file->move($path, $fileName);
  return $fileName;
}

if (!function_exists('storage_path')) {
  function storage_path($path = '')
  {
    return app()->basePath().'/storage'.($path ? '/'.$path : $path);
  }
}
if ( ! function_exists('config_path'))
{
  function config_path($path = '')
  {
    return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
  }
}
