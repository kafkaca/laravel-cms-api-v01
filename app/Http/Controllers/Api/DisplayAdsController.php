<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

class DisplayAdsController extends Controller
{
  public $datam = [];
  public function __construct(){
    $this->datam = app('request')->all();
  }

  public function displayAd(){
    /* client tarafından gelen tüm datalar */
    //data-info="id,user_id,kampanya_id,reklam_id"
    $native = $this->datam;
    $response = [];
    /* client tarafından gelen query encode ediliyor */
    $data = (array) json_decode(base64_decode($native['sor']));
    /* client tarafından gelen reklam_id veritabanından isteniyor */
    $db_reklam = app('db')->table('reklamlar')->where('id', $data['reklam_id'])->where('kampanya_id', $data['kampanya_id'])->first();
    if ($db_reklam) {
      /* veritabanındaki ad_body elemanı unserialize ediliyor */
      $ad_body = unserialize($db_reklam->ad_body); //$ad_body['imaj']
      /* client user_id ile veritabanı user_id eşleşiyormu ? */
      $response['image'] = "http://adsdemo.dev/uploads/images/". $ad_body['imaj'];
    }

    return response()->json($response, 200);
  }


  public function displayAdDeney(){

    $response = app('cache')->get('963852');
    if (!$response) {
      /* client tarafından gelen tüm datalar */
      //data-info="id,user_id,kampanya_id,reklam_id"
      $native = $this->datam;
      $response = [];
      /* client tarafından gelen query encode ediliyor */
      $data = (array) json_decode(base64_decode($native['sor']));
      $db_reklam = app('db')->table('reklamlar')->where('id', $data['reklam_id'])->where('kampanya_id', $data['kampanya_id'])->first();
      if ($db_reklam) {
        /* veritabanındaki ad_body elemanı unserialize ediliyor */
        $ad_body = unserialize($db_reklam->ad_body); //$ad_body['imaj']
        /* client user_id ile veritabanı user_id eşleşiyormu ? */
          $response['ad_body'] = $ad_body;
          $response['ad_type'] = $db_reklam->type;
          $response['kampanya_id'] = $db_reklam->kampanya_id;
          $response['count_id'] = $db_reklam->count_id;
          $response['kural_id'] = $db_reklam->kural_id;
          $response['status'] = $db_reklam->status;
      }
      app('cache')->put('963852', $response, 3600);
      return response()->json($response, 200);
    }

      //var_dump($this->datam);
    return response()->json($response, 200);
  }

  public function displayAdPost(){
    $native = $this->datam;
    $native['server'] = 1;
    return response()->json($native, 200);
  }

  public function allCampaigns()
  {
    $data = [];
    $data['user_id'] = 1;
    $data['kampanya_id'] = 1;

$response = app('db')->table('kampanyalar')->where('user_id', $data['user_id'])->paginate(2);
    return response()->json($response, 200);
  }

  public function fakeList()
  {
    $getArticles = file_get_contents("http://restwp.dev/wp-json/wp/v2/articles");
    $data = json_decode($getArticles);
    return response()->json($data, 200);
  }

  public function fakeItem($item)
  {
    $getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/articles/" . $item);
    $data = json_decode($getArticle);
    return response()->json($data, 200);
  }
}
