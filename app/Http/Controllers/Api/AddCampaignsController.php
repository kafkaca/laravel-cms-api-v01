<?php namespace App\Http\Controllers\Api;

//use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddCampaignsController extends Controller
{
  public function __construct(){

  }

  public function addCampaign(Request $request){
    $insert_array = $request->all();
    app('db')->table('kampanyalar')->insert($insert_array);
return response()->json($insert_array, 200);
    //app('db')->insert('insert into kampanyalar (name, type, note, user_id) values (?, ?, ?, ?)', array_values($request->all()));
  }

  public function updateCampaign(Request $request){
    // {
    //   "id" : 2,
    //   "user_id" : 1,
    //   "name" : "bir kampanya2",
    //   "type" : "banner",
    //   "note" : "bir notum",
    //   "status" : 1,
    //   "kolleksiyon" : 5
    // }

    $update_array = $request->all();
    app('db')->table('kampanyalar')
    ->where('id', $request->id)
    ->where('user_id', $request->user_id)
    ->update($update_array);

    //  app('db')->insert('insert into kampanyalar (name, type, note, user_id) values (?, ?, ?, ?)', array_values($request->all()));
  }
}
