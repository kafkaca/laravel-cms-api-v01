<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
class UserController extends Controller
{
    private $salt;
    public function __construct()
    {
        $this->salt="userloginregister";
    }
    public function login(Request $request){
      if ($request->has('username') && $request->has('password')) {
        $user = User:: where("username", "=", $request->input('username'))
                      ->where("password", "=", sha1($request->input('password')))
                      ->first();
        if ($user) {
          $token=str_random(60);
          $user->api_token=$token;
          $user->save();
          $request->session()->put('user', $user);
          return redirect('/admin');
        //  return $user->api_token;
        } else {
          return "hata user！";
        }
      } else {
        print_r($request->all());
      }
    }
    public function register(Request $request){
      if ($request->has('username') && $request->has('password') && $request->has('email')) {
        $user = new User;
        $user->username=$request->input('username');
        $user->password=sha1($request->input('password'));
        $user->email=$request->input('email');
        $user->api_token=str_random(60);
        if($user->save()){
          return "save！";
        } else {
          return "no save！";
        }
      } else {
        return "hata form！";
      }
    }
    public function info(){
      return Auth::user();
    }
}
