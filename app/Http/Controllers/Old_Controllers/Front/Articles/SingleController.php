<?php
namespace App\Http\Controllers\Front\Articles;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Storage;
// use Illuminate\Support\Facades\File;
// use Illuminate\Support\Facades\Cache;
class SingleController extends Controller
{
  protected $redirectTo = '/home';

  public function __construct()
  {
    // $this->middleware('guest');
  }

  public function index(Request $request)
  {
    return view('singles.empty');
  }

  public function single($id)
  {
  $posts = DB::connection('wordpress')->table('posts')->where('post_type', 'articles')->where('post_status', 'publish')->first();
    // $data = [];
    // $getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/articles/". $id);
    // $data['article'] = json_decode($getArticle);
    $data['article'] = $posts;
    return view('singles.article', $data);
  }
  public function course($id)
  {
    $data = [];
    $getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/courses/". $id);
    $data['article'] = json_decode($getArticle);
    return view('singles.course', $data);
  }
  public function classified($id)
  {
    $data = [];
    $getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/classified/". $id);
    $data['article'] = json_decode($getArticle);
    return view('singles.classified', $data);
  }

}
//  return response()->json(json_decode($data), 200);
