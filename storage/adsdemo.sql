-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 14, 2017 at 06:41 AM
-- Server version: 10.2.3-MariaDB-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adsdemo`
--

-- --------------------------------------------------------

--
-- Table structure for table `kampanyalar`
--

CREATE TABLE `kampanyalar` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `type` varchar(40) NOT NULL,
  `note` tinytext DEFAULT NULL,
  `user_id` smallint(6) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `count_id` smallint(6) DEFAULT 0,
  `kolleksiyon` smallint(6) DEFAULT 0,
  `olusturma` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kampanyalar`
--

INSERT INTO `kampanyalar` (`id`, `name`, `type`, `note`, `user_id`, `status`, `count_id`, `kolleksiyon`, `olusturma`) VALUES
(1, 'kampanya ilk', 'banner', 'bir notum', 1, 1, 0, 5, '2017-06-07 21:51:58'),
(3, 'bir çanta sezonu', 'banner', 'çanta sezonu açıklama', 1, 0, 0, 0, '2017-06-07 21:51:58'),
(4, 'telefon kampanyası', 'banner', 'telefon kapmyanya açıklama', 1, 0, 0, 0, '2017-06-07 21:51:58'),
(6, 'bir reklam kampanayası', 'banner', 'kampanya notları', 1, 0, 0, 0, '2017-06-07 22:18:52'),
(7, 'bir kampanya adı', 'banner', 'notlarım', 1, 0, 0, 0, '2017-06-07 23:14:39'),
(8, 'bir izmir kampanyası', 'banner', 'not açıklaması', 1, 0, 0, 0, '2017-06-08 22:36:05'),
(9, '0311 kampanyası', 'feed', 'notlarım', 2, 0, 0, 0, '2017-06-09 00:11:32'),
(10, 'merhaba kampanya angular', 'banner', 'kampanya açıklama angular', 0, 0, 0, 0, '2017-06-12 13:34:11'),
(11, 'merhaba kampanya angular', 'banner', 'kampanya açıklama angular', 0, 0, 0, 0, '2017-06-12 13:34:21'),
(12, 'merhaba kampanya angular', 'banner', 'kampanya açıklama angular', 0, 0, 0, 0, '2017-06-12 13:34:21'),
(13, 'merhaba kampanya angular', 'banner', 'kampanya açıklama angular', 0, 0, 0, 0, '2017-06-12 13:34:22'),
(14, 'benim başlığım bir kampanya', 'banner', 'asdsfd', 1, 0, 0, 0, '2017-06-12 13:44:13'),
(15, 'gökhan çelik', 'banner', 'dfgfdgfd', 1, 0, 0, 0, '2017-06-12 14:21:23'),
(16, 'gece tarifesi', 'banner', 'bir gece tarifesi açıklaması', 1, 0, 0, 0, '2017-06-12 20:40:13');

-- --------------------------------------------------------

--
-- Table structure for table `kurallar`
--

CREATE TABLE `kurallar` (
  `id` int(11) NOT NULL,
  `camp_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `kural_array` text DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kurallar`
--

INSERT INTO `kurallar` (`id`, `camp_id`, `user_id`, `kural_array`, `status`) VALUES
(1, 12, 10, 'a:14:{s:11:"lokasyonlar";a:4:{i:0;s:2:"tr";i:1;s:2:"ru";i:2;s:2:"fr";i:3;s:7:"Germany";}s:6:"diller";a:4:{i:0;s:8:"Türkçe";i:1;s:7:"English";i:2;s:6:"French";i:3;s:7:"Russian";}s:8:"browsers";a:4:{i:0;s:6:"Chrome";i:1;s:7:"Firefox";i:2;s:6:"Yandex";i:3;s:17:"Internet Explorer";}s:7:"notwork";a:4:{i:0;s:5:"Mobil";i:1;s:4:"Wifi";i:2;s:3:"Lan";i:3;s:8:"Intranet";}s:7:"tiklama";s:4:"1500";s:8:"gosterim";s:6:"150000";s:10:"sayim_turu";s:1:"1";s:10:"start_date";s:10:"2017-06-16";s:8:"end_date";s:10:"2017-06-30";s:10:"start_time";s:5:"02:00";s:8:"end_time";s:5:"02:00";s:7:"camp_id";s:2:"12";s:7:"user_id";s:2:"10";s:6:"status";s:1:"1";}', 1),
(2, 12, 10, 'a:14:{s:11:"lokasyonlar";a:3:{i:0;s:2:"tr";i:1;s:2:"ru";i:2;s:7:"Germany";}s:6:"diller";a:2:{i:0;s:8:"Türkçe";i:1;s:7:"English";}s:8:"browsers";a:3:{i:0;s:6:"Chrome";i:1;s:7:"Firefox";i:2;s:17:"Internet Explorer";}s:7:"notwork";a:3:{i:0;s:5:"Mobil";i:1;s:4:"Wifi";i:2;s:8:"Intranet";}s:7:"tiklama";s:0:"";s:8:"gosterim";s:0:"";s:10:"sayim_turu";s:1:"1";s:10:"start_date";s:0:"";s:8:"end_date";s:0:"";s:10:"start_time";s:5:"00:00";s:8:"end_time";s:5:"00:00";s:7:"camp_id";s:2:"12";s:7:"user_id";s:2:"10";s:6:"status";s:1:"1";}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_05_16_034905_create_users_table', 1),
(2, '2017_05_22_121851_create_user_metas_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reklamlar`
--

CREATE TABLE `reklamlar` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `ad_body` text DEFAULT NULL,
  `kampanya_id` smallint(6) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `kural_id` smallint(6) DEFAULT 0,
  `count_id` int(11) DEFAULT 0,
  `note` tinytext DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reklamlar`
--

INSERT INTO `reklamlar` (`id`, `name`, `ad_body`, `kampanya_id`, `type`, `kural_id`, `count_id`, `note`, `status`, `user_id`) VALUES
(2, 'ayakkabı kampanyası', 'a:5:{s:4:"imaj";s:0:"";s:4:"size";s:7:"300x250";s:10:"target_url";s:7:"http://";s:5:"ad_id";i:136;s:7:"alt_tag";s:9:"alt title";}', 1, 'banner', 0, 0, 'notlar açıklama', 0, 1),
(3, 'Banner Reklam Adı', 'a:5:{s:4:"imaj";s:10:"963852.jpg";s:4:"size";s:7:"300x250";s:10:"target_url";s:7:"http://";s:5:"ad_id";i:136;s:7:"alt_tag";s:9:"alt title";}', 1, 'banner', 0, 0, 'reklam notu', 0, 1),
(4, 'Banner Reklam Adı', 'a:4:{s:4:"imaj";s:10:"741852.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:20:"http://www.alexa.com";s:7:"alt_tag";s:18:"Banner Reklam Adı";}', 1, 'banner', 0, 0, 'reklam notu', 0, 1),
(5, 'Banner Reklam Adı Default', 'a:4:{s:4:"imaj";s:11:"default.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:20:"http://www.alexa.com";s:7:"alt_tag";s:26:"Banner Reklam Adı Default";}', 1, 'banner', 0, 0, 'notlarım', 0, 1),
(6, 'reklam adı', 'a:4:{s:4:"imaj";s:11:"healthy.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:11:"reklam adı";}', 1, 'banner', 0, 0, 'reklam notu', 0, 1),
(7, 'controle reklam', 'a:4:{s:4:"imaj";s:10:"741852.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:15:"controle reklam";}', 1, 'banner', 0, 0, 'not açıklama', 0, 1),
(18, 'reklam adi var', 'a:5:{s:4:"imaj";s:72:"c2NyZWVuc2hvdC0xMjcuMC4wLjEtODg4Ny0yMDE3LTA1LTIyLTA4LTU2LTQxLmpwZw==.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:14:"reklam adi var";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'notlar var', 0, 1),
(22, 'bir reklam daha', 'a:5:{s:4:"imaj";s:69:"c2NyZWVuc2hvdC1saWJlcnllbi5kZXYtMjAxNy0wNS0wOC0wMy00My0xOC5qcGVn.jpeg";s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:15:"bir reklam daha";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'buda notları', 0, 1),
(23, 'Banner Reklam Adı', 'a:5:{s:4:"imaj";s:20:"b25saW5lLlBORw==.PNG";s:4:"size";s:6:"banner";s:10:"target_url";s:20:"http://www.alexa.com";s:7:"alt_tag";s:18:"Banner Reklam Adı";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'notlarım', 0, 1),
(24, 'Banner Reklam Adı', 'a:5:{s:4:"imaj";s:20:"b25saW5lLlBORw==.PNG";s:4:"size";s:6:"banner";s:10:"target_url";s:20:"http://www.alexa.com";s:7:"alt_tag";s:18:"Banner Reklam Adı";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'notlarım', 0, 1),
(25, 'book reklam', 'a:5:{s:4:"imaj";s:16:"MDAwMDkuanBn.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:11:"book reklam";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 's f f dsf sdf ', 0, 1),
(26, 'book reklam', 'a:5:{s:4:"imaj";s:16:"MDAwMDcuanBn.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:11:"book reklam";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 's f f dsf sdf ', 0, 1),
(27, 'sarı reklam', 'a:5:{s:4:"imaj";s:16:"MDAwMTAuanBn.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:12:"sarı reklam";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'not sarı', 0, 1),
(28, 'havlu reklamı', 'a:5:{s:4:"imaj";s:16:"MDAwMTIuanBn.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:14:"havlu reklamı";s:4:"type";s:6:"banner";}', 8, 'banner', 0, 0, 'havlu açıklama', 0, 1),
(29, '3:52 reklam 46', 'a:5:{s:4:"imaj";s:16:"MDAwMTIuanBn.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:20:"http://www.alexa.com";s:7:"alt_tag";s:14:"3:52 reklam 46";s:4:"type";s:6:"banner";}', 9, 'banner', 0, 0, 'fdgfgdfgdf', 0, 2),
(30, 'bir baska kampanya 04:04', 'a:5:{s:4:"imaj";s:16:"MDAwMDYuanBn.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:24:"bir baska kampanya 04:04";s:4:"type";s:6:"banner";}', 9, 'banner', 0, 0, 'merhaba ', 0, 2),
(31, 'gökhan çelik', 'a:4:{s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:14:"gökhan çelik";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'dhfghfg', 0, 1),
(32, 'angular reklam', 'a:4:{s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:14:"angular reklam";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'dhfghfg', 0, 1),
(33, 'camp 1  add item', 'a:4:{s:4:"size";s:6:"banner";s:10:"target_url";s:20:"http://www.alexa.com";s:7:"alt_tag";s:16:"camp 1  add item";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'camp 1 notlar', 0, 1),
(34, 'camp 1  add item', 'a:4:{s:4:"size";s:6:"banner";s:10:"target_url";s:20:"http://www.alexa.com";s:7:"alt_tag";s:16:"camp 1  add item";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'camp 1  add item', 0, 1),
(35, 'gökhan çelik file test', 'a:4:{s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:24:"gökhan çelik file test";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'fdf sd fgdsf s', 0, 1),
(36, 'benim başlığım bir kampanya file test 0000000000', 'a:4:{s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:52:"benim başlığım bir kampanya file test 0000000000";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'cx cv c ', 0, 1),
(37, 'benim başlığım bir kampanya file test 0000000000', 'a:4:{s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:52:"benim başlığım bir kampanya file test 0000000000";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'cx cv c ', 0, 1),
(38, 'benim başlığım bir kampanya file test 0000000000', 'a:4:{s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:52:"benim başlığım bir kampanya file test 0000000000";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'cx cv c ', 0, 1),
(39, 'benim başlığım bir kampanya file test 0000000000', 'a:4:{s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:52:"benim başlığım bir kampanya file test 0000000000";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'cx cv c ', 0, 1),
(40, 'gökhan çelik 963', 'a:4:{s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:18:"gökhan çelik 963";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, 'gfdg dg d ', 0, 1),
(41, 'gökhan çelik 741852', 'a:5:{s:4:"imaj";s:9:"indir.jpg";s:4:"size";s:6:"banner";s:10:"target_url";s:21:"http://www.google.com";s:7:"alt_tag";s:21:"gökhan çelik 741852";s:4:"type";s:6:"banner";}', 1, 'banner', 0, 0, ' gfgf f', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `ad_id` int(11) DEFAULT NULL,
  `camp_id` int(11) DEFAULT NULL,
  `click` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `api_token` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `role`, `password`, `api_token`, `remember_token`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'kafkadeveloper', 'kafkadeveloper@gmail.com', '', '$2y$10$N5kw4Pf0fbW0tGeJoZJgYOgqgrqi21vF0ZC8bmKGXC4fS11BB9GFC', NULL, NULL, '', '2017-06-01 15:15:37', '2017-06-01 15:15:37', NULL),
(2, 'user', 'user@example.com', '', '$2y$10$.W8aqOXqpeHJnbLADEsZ0e3egga7oYFHexHRgC15bhe5SVkeG74/W', NULL, NULL, '', '2017-06-01 15:15:37', '2017-06-01 15:15:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_metas`
--

CREATE TABLE `user_metas` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `meta_name` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kampanyalar`
--
ALTER TABLE `kampanyalar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `kurallar`
--
ALTER TABLE `kurallar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `camp_id` (`camp_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reklamlar`
--
ALTER TABLE `reklamlar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kampanya_id` (`kampanya_id`),
  ADD KEY `kampanya_id_2` (`kampanya_id`),
  ADD KEY `type` (`type`),
  ADD KEY `kural_id` (`kural_id`),
  ADD KEY `count_id` (`count_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ad_id_2` (`ad_id`),
  ADD KEY `camp_id` (`camp_id`),
  ADD KEY `ad_id` (`ad_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_metas`
--
ALTER TABLE `user_metas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kampanyalar`
--
ALTER TABLE `kampanyalar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `kurallar`
--
ALTER TABLE `kurallar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reklamlar`
--
ALTER TABLE `reklamlar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_metas`
--
ALTER TABLE `user_metas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
